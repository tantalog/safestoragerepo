import UIKit


class ViewController: UIViewController {
    
    @IBOutlet weak var addPictureButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!

    let imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.imagePicker.delegate = self
        let tap = UITapGestureRecognizer(target: self, action: #selector(moveToLoadPicturesController(touch:)))
        tap.numberOfTapsRequired = 1

    }
    
    
    @IBAction func addPictureButtonPressed(_ sender: UIButton) {
        self.imagePicker.sourceType = .photoLibrary
        present(self.imagePicker, animated: true, completion: nil)
    }

    
    
    @objc func moveToLoadPicturesController(touch: UITapGestureRecognizer) {
        guard let scrollPicturesController = self.storyboard?.instantiateViewController(withIdentifier: "ScrollPictuersController") as? ScrollPictuersController else {
            return
        }
        self.navigationController?.pushViewController(scrollPicturesController, animated: true)
    }
    
    
    func appendImageNametoSavedImages(image: UIImage) {
        let imagelink = StorageManager.shared.saveImage(image: image)
        let savedImage = SavedPictures(fileName: imagelink, isLiked: false)
        StorageManager.shared.savePicturesArray(savedPictures: savedImage)
    }

    
}


extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            self.appendImageNametoSavedImages(image: pickedImage)
            picker.dismiss(animated: true, completion: nil)
            self.collectionView.reloadData()
        }
    }
}





extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let array = StorageManager.shared.loadPicturesArray()
        return array.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomCollectionViewCell", for: indexPath) as? CustomCollectionViewCell else {
            return UICollectionViewCell()
        }
        let array = StorageManager.shared.loadPicturesArray()
        cell.configure(object: array[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let side = self.collectionView.frame.width/2 - 5
        return CGSize(width: side, height: side)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let scrollPicturesController = self.storyboard?.instantiateViewController(withIdentifier: "ScrollPictuersController") as? ScrollPictuersController else {
            return
        }
        self.navigationController?.pushViewController(scrollPicturesController, animated: true)
    }
}

