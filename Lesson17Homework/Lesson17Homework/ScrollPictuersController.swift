import UIKit

class ScrollPictuersController: UIViewController {
    
    @IBOutlet weak var firstImageView: UIImageView!
    @IBOutlet weak var commentsTextField: UITextField!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var containerView: UIView!
    
    let moveDistance: CGFloat = 700
    var loadedImagesArray: [SavedPictures] = []
    var count = 0
    var timer: TimeInterval = 0.3
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadedImagesArray = StorageManager.shared.loadPicturesArray()
        self.registerForKeyboardNotifications()
        self.createSwipeRecognizer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.showNextImage(onView: self.view.frame.width)
        
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.backAction()
    }
    
    
    @IBAction func rightSwipeDetected(_ recognizer: UISwipeGestureRecognizer) {
        self.count += 1
        if self.count == self.loadedImagesArray.count {
            self.count = 0
        }
        self.showNextImage(onView: self.view.frame.width)
    }
    
    @IBAction func leftSwipeDetected(_ recognizer: UISwipeGestureRecognizer) {
        if self.count == 0 {
            self.count = self.loadedImagesArray.count
        }
        self.count -= 1
        self.showNextImage(onView: -self.view.frame.width)
    }
    
    
    @IBAction func likeButtonPressed(_ sender: UIButton) {
        self.like()
        self.changeLikeButtonColor()
    }
    
    @objc func showNextImage(onView: CGFloat) {
        let nextImageView = UIImageView()
        let currentImageName = self.loadedImagesArray[count].fileName
        if let currentImageName = currentImageName {
            nextImageView.image =  StorageManager.shared.loadImage(fileName: currentImageName)
        }
        nextImageView.frame = self.firstImageView.frame
        nextImageView.frame.origin.x += onView
        nextImageView.contentMode = self.firstImageView.contentMode
        
        self.containerView.addSubview(nextImageView)
        
        UIView.animate(withDuration: timer, animations:  {
            nextImageView.frame = self.firstImageView.frame
            self.changeLikeButtonColor()
            self.commentsTextField.text = self.loadedImagesArray[self.count].comment
        }, completion: { (_) in
            self.firstImageView.image = nextImageView.image
            nextImageView.removeFromSuperview()
        })
    }
    
    func createSwipeRecognizer() {
        let rightSwipeRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(rightSwipeDetected))
        rightSwipeRecognizer.direction = .right
        self.view.addGestureRecognizer(rightSwipeRecognizer)
        let leftSwipeRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(leftSwipeDetected))
        leftSwipeRecognizer.direction = .left
        self.view.addGestureRecognizer(leftSwipeRecognizer)
    }
    
    func like() {
        if self.loadedImagesArray[count].isLiked == false {
            self.loadedImagesArray[count].isLiked = true
        } else {
            self.loadedImagesArray[count].isLiked = false
        }
        StorageManager.shared.updatePicturesArray(with: loadedImagesArray)
    }
    
    func changeLikeButtonColor() {
        if self.loadedImagesArray[count].isLiked == false {
            self.likeButton.tintColor = .lightGray
        } else {
            self.likeButton.tintColor = .blue
        }
    }
    
    private func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func keyboardWillShow(_ notification: NSNotification) {
        
        let userInfo = notification.userInfo!
        let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            self.bottomConstraint.constant = 0
        } else {
            self.bottomConstraint.constant = keyboardScreenEndFrame.height
            self.scrollView.contentOffset = CGPoint(x: 0, y: self.scrollView.contentSize.height)
        }
        self.view.needsUpdateConstraints()
        
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func backAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
}


extension ScrollPictuersController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.loadedImagesArray[count].comment = commentsTextField.text
        StorageManager.shared.updatePicturesArray(with: loadedImagesArray)
        return true
    }
    
    func hideKeyboard () {
        let recognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(recognizer)
    }
    @objc func dismissKeyboard () {
        view.endEditing(true)
    }
}
