import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    func configure(object: SavedPictures) {
        
        if let object = object.fileName {
            let image = StorageManager.shared.loadImage(fileName: object)
            self.imageView.image = image
        }
    }
    
}
