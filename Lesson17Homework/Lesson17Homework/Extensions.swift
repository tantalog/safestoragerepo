import Foundation
import UIKit

extension LoginViewController: UITextFieldDelegate {
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func hideKeyboard () {
        let recognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(recognizer)
    }
    @objc func dismissKeyboard () {
        view.endEditing(true)
    }
}

extension RegistrationViewController: UITextFieldDelegate {
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func hideKeyboard () {
        let recognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(recognizer)
    }
    @objc func dismissKeyboard () {
        view.endEditing(true)
    }
}

extension ViewController: UITextFieldDelegate {
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func hideKeyboard () {
        let recognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(recognizer)
    }
    @objc func dismissKeyboard () {
        view.endEditing(true)
    }
}

extension UIViewController {
    func errorAlert () {
        let alert = UIAlertController(title: "Error", message: "Please, enter a data", preferredStyle: .alert)
        let firstAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
           alert.addAction(firstAction)
           self.present(alert, animated: true, completion: nil)
       }
    
    func errorRegistration () {
           let alert = UIAlertController(title: "Registration canceled", message: nil, preferredStyle: .alert)
           let firstAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
              alert.addAction(firstAction)
              self.present(alert, animated: true, completion: nil)
          }
    
    func errorUserAlreadyExistAlert () {
     let alert = UIAlertController(title: "User already exist", message: "If you forgot password restore it", preferredStyle: .alert)
     let firstAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(firstAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func errorAddUser () {
     let alert = UIAlertController(title: "Something went wrong", message: "To enter, please, register", preferredStyle: .alert)
     let firstAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(firstAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func errorPasswordIncorrect () {
     let alert = UIAlertController(title: "Data is incorrect", message: "Please, check it", preferredStyle: .alert)
     let firstAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(firstAction)
        self.present(alert, animated: true, completion: nil)
    }
}

extension UserDefaults {
    
    func set<T: Encodable>(encodable: T, forKey key: String) {
        if let data = try? JSONEncoder().encode(encodable) {
            set(data, forKey: key)
        }
    }
    
    func value<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
        if let data = object(forKey: key) as? Data,
            let value = try? JSONDecoder().decode(type, from: data) {
            return value
        }
        return nil
    }
}


extension UIView {
    
    
    func addGradientThreeColors(firstColor: UIColor, secondColor: UIColor, thirdColor: UIColor, opacity: Float, startPoint: CGPoint, endPoint: CGPoint, radius: CGFloat) {
        let gradient = CAGradientLayer()
        gradient.colors = [firstColor.cgColor, secondColor.cgColor, thirdColor.cgColor]
        gradient.opacity = opacity
        gradient.startPoint = startPoint
        gradient.endPoint = endPoint
        gradient.cornerRadius = radius
        gradient.frame = self.bounds
        self.layer.addSublayer(gradient)
    }
}

