import UIKit
import SwiftyKeychainKit

class LoginViewController: UIViewController {

    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    
    @IBOutlet weak var restoreButton: UIButton!
    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet weak var registrationView: UIView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.usernameTextField.delegate = self
        self.passwordTextField.delegate = self
        self.hideKeyboard()
        
    }
    
    override func viewDidLayoutSubviews() {
        self.backGroundView.addGradientThreeColors(firstColor: .black, secondColor: .white, thirdColor: .black, opacity: 1, startPoint: CGPoint(x: 0.0, y: 1.0), endPoint: CGPoint(x: 1.0, y: 0.0), radius: 0)
    }
    
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        
        let tryData = try? StorageManager.shared.keychain.get(KeychainKeys.usernameAccessTokenKey)
        if tryData == nil {
                   self.errorAddUser()
        } else {
            guard let username = self.usernameTextField.text else {return}
            guard let password = self.passwordTextField.text else {return}
            
            guard let saveUsername = try? StorageManager.shared.keychain.get(KeychainKeys.usernameAccessTokenKey) else {return}
            guard let savePassword = try? StorageManager.shared.keychain.get(KeychainKeys.passwordAccessTokenKey) else {return}
            
            if username == saveUsername && password == savePassword {
                guard let viewCotroller = self.navigationController?.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController else { return }
                self.navigationController?.pushViewController(viewCotroller, animated: true)
                
            } else if username != saveUsername && password != savePassword {
                self.errorPasswordIncorrect()
            }
        }
    }
    
    @IBAction func registerButtonPressed(_ sender: UIButton) {
        
        let saveUsername = try? StorageManager.shared.keychain.get(KeychainKeys.usernameAccessTokenKey)
        
        if saveUsername == nil {
            guard let registrationController = self.navigationController?.storyboard?.instantiateViewController(withIdentifier: "RegistrationViewController") as? RegistrationViewController else { return }
            self.navigationController?.pushViewController(registrationController, animated: true)
        } else {
            self.errorUserAlreadyExistAlert()
        }
    }
    
    @IBAction func restoreButtonPressed(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Password recovery", message: "Answer security questions", preferredStyle: .alert)
        
        let label = UILabel(frame: CGRect(x: 0, y: 40, width: 270, height:18))
        label.text = "Please, enter a data"
        label.textAlignment = .center
        label.textColor = .red
        label.font = label.font.withSize(12)
        alert.view.addSubview(label)
        label.isHidden = true
        
        alert.addTextField { (textFieldOne) in
         textFieldOne.placeholder = try? StorageManager.shared.keychain.get(KeychainKeys.firstSecurityQuestion)
        }
        alert.addTextField { (textFieldTwo) in
            textFieldTwo.placeholder = try? StorageManager.shared.keychain.get(KeychainKeys.secondSecurityQuestion)
        }
        alert.addTextField { (textFieldThree) in
            textFieldThree.placeholder = try? StorageManager.shared.keychain.get(KeychainKeys.thirdSecurityQuestion)
        }
        
        let actionContinue = UIAlertAction(title: "Continue", style: .default, handler: { (action) -> Void in
            
            let textFieldOne = alert.textFields![0]
            let textFieldTwo = alert.textFields![1]
            let textFieldThree = alert.textFields![2]
            
            if let userInputOne = textFieldOne.text, let userInputTwo = textFieldTwo.text, let userInputThree = textFieldThree.text {
                if userInputOne.isEmpty == false , userInputTwo.isEmpty == false, userInputThree.isEmpty == false {
                    label.isHidden = true
                 
                 guard let firstAnswer = try? StorageManager.shared.keychain.get(KeychainKeys.firstSecurityQuestionAnswer) else {return}
                 guard let secondAnswer = try? StorageManager.shared.keychain.get(KeychainKeys.secondSecurityQuestionAnswer) else {return}
                 guard let thirdAnswer = try? StorageManager.shared.keychain.get(KeychainKeys.thirdSecurityQuestionAnswer) else {return}
                 
                 if userInputOne == firstAnswer && userInputTwo == secondAnswer && userInputThree == thirdAnswer {
                    
                    self.getNewPassword()
                 }
                 
                } else {
                    label.isHidden = false
                    self.present(alert, animated: true, completion: nil)
                }
            }
            
        })
        
        let actionCancel = UIAlertAction(title: "Cancel", style: .default, handler: { (action) -> Void in
        })
        
        alert.addAction(actionContinue)
        alert.addAction(actionCancel)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func getNewPassword() {
        let alert = UIAlertController(title: "Password recovery", message: "Enter new password", preferredStyle: .alert)
        alert.addTextField { (textFieldOne) in
         textFieldOne.placeholder = "Enter new password"
        }
        let actionContinue = UIAlertAction(title: "Continue", style: .default, handler: { (action) -> Void in
            let textFieldOne = alert.textFields![0]
            guard let password = textFieldOne.text else {return}
            try? StorageManager.shared.keychain.set(password, for: KeychainKeys.passwordAccessTokenKey)
    })
        alert.addAction(actionContinue)
        self.present(alert, animated: true, completion: nil)
    }
    

}

