
class SavedPictures: Codable {
    var fileName: String?
    var comment: String?
    var isLiked: Bool

    private enum CodingKeys: String, CodingKey {
        case fileName
        case comment
        case isLiked
    }
   
    init (fileName: String?, isLiked: Bool) {
        self.fileName = fileName
        self.isLiked = isLiked
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        fileName = try container.decodeIfPresent(String.self, forKey: .fileName)
        comment = try container.decodeIfPresent(String.self, forKey: .comment)
        isLiked = try container.decode(Bool.self, forKey: .isLiked)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(self.fileName, forKey: .fileName)
        try container.encode(self.comment, forKey: .comment)
        try container.encode(self.isLiked, forKey: .isLiked)
    }

}
