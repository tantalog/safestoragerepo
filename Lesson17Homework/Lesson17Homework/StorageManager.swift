import Foundation
import UIKit
import SwiftyKeychainKit

class StorageManager {
    
    enum Keys: String {

        case imagesArray = "imagesArray"
    }

    static let shared = StorageManager()
    
    private init() {}
    private let defaults = UserDefaults.standard
    let keychain = Keychain(service: "com.swifty.keychain")

    
    func savePicturesArray (savedPictures: SavedPictures) {
           var savedPicturesArray = StorageManager.shared.loadPicturesArray()
           savedPicturesArray.append(savedPictures)
           defaults.set(encodable: savedPicturesArray, forKey: Keys.imagesArray.rawValue)
       }
    
    func loadPicturesArray() -> [SavedPictures] {
        let savedPicturesArray = defaults.value([SavedPictures].self, forKey: Keys.imagesArray.rawValue)
        if let savedPicturesArray = savedPicturesArray {
            return savedPicturesArray
        } else  {
            return []
        }
    }
    
    func clearPicturesArray() {
        var savedPicturesArray = StorageManager.shared.loadPicturesArray()
        savedPicturesArray = []
        defaults.set(encodable: savedPicturesArray, forKey: Keys.imagesArray.rawValue)
    }
    
    func updatePicturesArray(with array: [SavedPictures]) {
        clearPicturesArray()
      defaults.set(encodable: array, forKey: Keys.imagesArray.rawValue)
    }
    
    func saveImage(image: UIImage) -> String? {
        guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil}
        let fileName = UUID().uuidString
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        guard let data = image.jpegData(compressionQuality: 1) else { return nil}
        if FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                try FileManager.default.removeItem(atPath: fileURL.path)
                print("Removed old image")
            } catch let removeError {
                print("couldn't remove file at path", removeError)
            }
        }
        do {
            try data.write(to: fileURL)
            return fileName
        } catch let error {
            print("error saving file with error", error)
            return nil
        }
    }
    
    func loadImage(fileName:String) -> UIImage? {
        let documentDirectory = FileManager.SearchPathDirectory.documentDirectory
        
        let userDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(documentDirectory, userDomainMask, true)
        
        if let dirPath = paths.first {
            let imageUrl = URL(fileURLWithPath: dirPath).appendingPathComponent(fileName)
            let image = UIImage(contentsOfFile: imageUrl.path)
            return image
        }
        return nil
    }
  
}

extension KeychainKeys {
    
    static let usernameAccessTokenKey = KeychainKey<String>(key: "usernameAccessTokenKey")
    static let passwordAccessTokenKey = KeychainKey<String>(key: "passwordAccessTokenKey")
    static let firstSecurityQuestion = KeychainKey<String>(key: "firstSecurityQuestion")
    static let secondSecurityQuestion = KeychainKey<String>(key: "secondSecurityQuestion")
    static let thirdSecurityQuestion = KeychainKey<String>(key: "thirdSecurityQuestion")
    
    static let firstSecurityQuestionAnswer = KeychainKey<String>(key: "firstSecurityQuestionAnswer")
    static let secondSecurityQuestionAnswer = KeychainKey<String>(key: "secondSecurityQuestionAnswer")
    static let thirdSecurityQuestionAnswer = KeychainKey<String>(key: "thirdSecurityQuestionAnswer")
}
